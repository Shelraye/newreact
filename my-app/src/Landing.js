import React, { useEffect, useState } from "react";


function ComponetOne(props) {
    const [prop1, setProp1] = useState();
    const [prop2, setProp2] = useState();

    const handleClick = () => setProp1()
}

function CompTwo(props) {
    const [things, setThings] = useState([]);
    const [error, setError] = useState('');

    useEffect(() => {
        async getData() {
            const url = `api url`;
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setThings(data);

            } else {
                setError('Unable to load');
            }
        }
    }, [setThings, setError])

}

export default CompTwo;